@extends('layouts.app')

@section('css')
    @parent
	<link type="text/css" rel="stylesheet" href="{{ asset('css/recipe.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/recipe_600.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('css/form.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/form_600.css') }}">
@endsection

@section('title', $recipe->title)

@section('content_class', 'recipe')
    		
@section('content')
    
    <div id="title">
		<h1>{{ $recipe->title }}</h1>
	</div>
	<div id="image">
		<img  alt="Imgage of {{ $recipe->url }}" src="{{ $recipe->imageurl }}"/>
	</div>
	<div id="summary">
		<div class="notes">
			<div class="note">
				<span class="title">Prep time</span>
				<span class="info">{{ $recipe->preptime }}</span>
			</div>
			<div class="note">
				<span class="title">Cook time</span>
				<span class="info">{{ $recipe->cooktime }}</span>
			</div>
			<div class="note">
				<span class="title">Servings</span>
				<span class="info">{{ $recipe->quantity }}</span>
			</div>
		</div>
		<p>
    		{{ $recipe->description }}
		</p>
	</div>
	<div id="ingredients">
		<h2>Ingredients</h2>
		<ul>
    		@foreach ($recipe->ingredients as $ingredient)
    		    <li>{{ $ingredient->ingredient }}</li>
    		@endforeach
		</ul>
	</div>
	<div id="instructions">
		<h2>Instructions</h2>
		<ol>
    		@foreach ($recipe->instructions as $instruction)
    		    <li>{{ $instruction->instruction }}</li>
    		@endforeach
		</ol>
	</div>
	<div id="comments">
		<h2>Comments</h2>
		@auth
		<form action="{{ route('comment.create', $name) }}" method="post">
    		@csrf
			<label for="comment">Comment:</label>
            @if ($errors->has('comment'))
                <span class="error">{{ $errors->first('comment') }}</span>
            @endif
			<textarea id="comment" name="comment" placeholder="Your message">{{ old('comment') }}</textarea>
			<input type="submit" value="Send" />
		</form>
		@endauth
		@foreach ($comments as $comment)
    		<div class="comment">
    			<span class="name">{{ $comment->user->name }}</span>
    			<span class="date">{{ $comment->created_at }}</span>
    			<p>
    				{{ $comment->comment }}
    			</p>
    			@if ($comment->user->id == Auth::id())
    			    <form action="{{ route('comment.destroy', [$name, $comment->id]) }}" method="post">
                        @csrf
        			    @method('DELETE')
                        <input type="submit" value="Delete comment"/>
    			    </form>
    			@endif
    		</div>
		@endforeach
	</div>
    
@endsection